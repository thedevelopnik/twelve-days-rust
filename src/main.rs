fn main() {
    let days: [(usize, &str, &str); 12] = [
      (1, "first", "a partridge in a pear tree"),
      (2, "second", "two turtle doves"),
      (3, "third", "three french hens"),
      (4, "fourth", "four calling birds"),
      (5, "fifth", "five golden rings"),
      (6, "sixth", "six geese a-laying"),
      (7, "seventh", "seven swans a-swimming"),
      (8, "eighth", "eight maids a-milking"),
      (9, "ninth", "nine ladies dancing"),
      (10, "tenth", "ten lords a-leaping"),
      (11, "eleventh", "eleven pipers piping"),
      (12, "twelfth", "twelve drummers drumming")
    ];

    for day in days.iter() {
        match day.0 {
            1 => {
                print_intro(day);
                print_true_love(day);
                println!("{}!\n", day.2)
            },
            _ => {
                print_intro(day);
                print_true_love(day);
                for i in (1..day.0).rev() {
                    print_partial_sentence(&days[i]);
                }
                print_partridge();
            }
        }
    }
}

fn print_intro(day: &(usize, &str, &str)) {
  println!("On the {} day of Christmas", day.1);
}

fn print_true_love(day: &(usize, &str, &str)) {
  println!("my true love gave to me,");
}

fn print_partial_sentence(day: &(usize, &str, &str)) {
  println!("{},", day.2);
}

fn print_partridge() {
  println!("and a partridge in a pear tree!\n");
}
